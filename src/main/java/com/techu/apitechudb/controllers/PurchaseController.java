package com.techu.apitechudb.controllers;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLOutput;
import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {
    @Autowired
    PurchaseService purchaseService;


    @PostMapping("/purchases")
    public ResponseEntity<String> addpurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addpurchase");
        System.out.println("La id de la nueva compra  es " + purchase.getId());
        System.out.println("La id del usuario de la nueva compra  es " + purchase.getUserId());
        System.out.println("Los productos a comprar son " + purchase.getPurchaseItems());

        int result = this.purchaseService.addpurchase(purchase);

        System.out.println("En purchase controller retorno " + result);
        if (result == 1) {
            System.out.println("En purchase contoller - usuario no registrado");
            return new ResponseEntity<>("Usuario no registrado",HttpStatus.NOT_FOUND);
        }
        if (result == 2) {
            System.out.println("En purchase contoller - compra ya existe");
            return new ResponseEntity<>("Existe una compra con ese id",HttpStatus.NOT_ACCEPTABLE);
        }
        if (result == 3) {
            System.out.println("En purchase contoller - producto no existe");
            return new ResponseEntity<>("No existe alguno de los productos",HttpStatus.NOT_FOUND);
        }
        System.out.println("En purchase controller - todo OK");
        return new ResponseEntity<>("Compra registrada", HttpStatus.OK);

    }


    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {

        System.out.println("getPurchases");
        List<PurchaseModel> result = this.purchaseService.findAll();
        return new ResponseEntity<>(
                result,HttpStatus.OK
        );
    }
}
