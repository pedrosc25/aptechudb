package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserRepository {

    public List<UserModel> findAll(int age) {
        System.out.println("findAll en UserRepository " + age);

// clausula de guarda:
        if (age < 0) {
            return ApitechudbApplication.userModels;
        }

        ArrayList<UserModel> result = new ArrayList<>();

        for (UserModel userInList : ApitechudbApplication.userModels) {
            if (userInList.getAge() == age || age == 0) {
                System.out.println("Usuario encontrado con la edad del parámetro " +
                        userInList.getName() + " tiene edad " + userInList.getAge());
                result.add(userInList);
            }
        }
        return result;

// función en forma de landa: función reducida a la mínima expresión:
//        return ApitechudbApplication.userModels.stream().
//                      filter(userModel -> userModel.getAge() == age).collect(Collectors.toList());
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("FindById en UserRepository " + id);
        Optional<UserModel> result = Optional.empty();
        for (UserModel userInList : ApitechudbApplication.userModels){
            if (userInList.getId().equals(id)) {
                System.out.println("Usuario encontrado en findById");
                result = Optional.of(userInList);
            }
        }
        return result;
    }

    public UserModel newuser (UserModel user) {
        System.out.println("newuser en UserRepository");
        ApitechudbApplication.userModels.add(user);
        return user;
    }

    public UserModel update (UserModel user) {
        System.out.println("update en UserRepository");
        Optional<UserModel> userToUpdate = this.findById(user.getId());

        if (userToUpdate.isPresent() == true){
            System.out.println("Usuario encontrado en if de UPDATE");
            UserModel userFromList = userToUpdate.get();
            userFromList.setName(user.getName());
            userFromList.setAge(user.getAge());
        }
        return user;
    }

    public void delete (UserModel user) {
        System.out.println("delete en UserRepository");
        System.out.println("borrando usuario");
        ApitechudbApplication.userModels.remove(user);
    }
}
